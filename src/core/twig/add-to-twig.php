<?php

// Dump to console
class consoleTwig {
    static function log() {
        echo '<script>console.log('.  self::implode(func_get_args())  .')</script>';
    }

    static function implode($args) {
    	$res = [];

    	foreach ($args as $arg) {
    		$res[] = json_encode($arg);
	    }

	    return implode(',', $res);
    }
}

// Add to twig
add_filter('get_twig', function ($twig) {
    $twig->addExtension( new Twig_Extension_StringLoader() );
    $twig->addFunction(new Twig_SimpleFunction('console', function() {
        consoleTwig::log(func_get_args());
    }));
    $twig->addFilter(new Twig_SimpleFilter('nl2p', 'nl2p'));
    return $twig;
});
