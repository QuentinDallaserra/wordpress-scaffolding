<?php

//Add to Twig context

function add_to_context( $context ) {
    $context['menuMain'] = new TimberMenu('main-menu');
    $context['menuFooter'] = new TimberMenu('footer-menu');
    $context['customPost'] = Timber::get_posts( array(
        'post_type'  => 'custom-post'
    ) );
    $context['tags'] = Timber::get_terms('tags');
    return $context;
}

add_filter('timber_context', 'add_to_context');
