<?php

function enqueue_scripts() {

    // Main Stylesheet
    wp_register_style( 'main-style', get_stylesheet_directory_uri() . '/style.css', [], filemtime(get_stylesheet_directory() . '/style.css'), false);
    wp_enqueue_style('main-style');

    // Google fonts
    $query_args = array(
        'family' => 'Barlow+Semi+Condensed:300,600,700,800',
    );
    wp_register_style( 'google-fonts', add_query_arg( $query_args, "//fonts.googleapis.com/css" ), array(), null );
    wp_enqueue_style('google-fonts');

    // jQuery
    wp_deregister_script('jquery-core');
    wp_register_script('jquery-core', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js", false, null);
    wp_enqueue_script('jquery-core');

    // Main JS Script
    wp_register_script( 'main-js', get_stylesheet_directory_uri() .'/main.js', [], filemtime(get_stylesheet_directory() . '/main.js'), 'jquery', '1.0', true );
    wp_enqueue_script('main-js');

    // Localise Scripts
    wp_localize_script( 'main-js', 'site', [
        'ajax' => admin_url( 'admin-ajax.php' ),
        'url'  => get_bloginfo('url'),
        'path' => get_bloginfo('template_directory')
    ]);

}

if (!is_admin()) {
    add_action( 'wp_enqueue_scripts', 'enqueue_scripts', 10 );
}
