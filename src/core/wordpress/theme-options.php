<?php

add_action('init', function () {
    // Theme suppport
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_theme_support('menus');
    add_theme_support('custom-logo', [
        'height' => 100,
        'flex-width' => true,
    ]);
    add_theme_support('post-formats', ['image', 'video', 'gallery', 'audio']);
    add_theme_support('html5', [
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
        'style',
        'script',
    ]);
    add_post_type_support('page', 'excerpt');

    // Google API Key for Google Maps
    add_filter('acf/settings/google_api_key', function () {
        return 'INSERT_API_KEY_HERE';
    });

    // Add theme options
    if (function_exists('acf_add_options_page')) {
        $option_page = acf_add_options_page([
            'page_title' => 'Site Options',
            'menu_title' => 'Site Options',
            'menu_slug' => 'site-options',
            'capability' => 'edit_posts',
            'redirect' => false,
            'post_id' => 'site-options',
        ]);
    }
});
