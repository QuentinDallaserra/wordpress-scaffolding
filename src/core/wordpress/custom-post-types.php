<?php

add_action('init', function () {

    register_post_type("custom-post", array(
        "labels" => array(
            "name" => "Custom Post",
            "singular_name" => "custom-post",
        ),
        "public" => true,
        "show_ui" => true,
        'show_in_rest' => true,
        'rest_base' => 'custom-post',
        'rest_controller_class' => 'WP_REST_Posts_Controller',
        "menu_icon" => "dashicons-plus",
        "supports" => array( "title", "thumbnail" ),
    ));

});
