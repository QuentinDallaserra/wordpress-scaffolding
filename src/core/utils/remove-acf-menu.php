<?php

// Removes the ACF menu (Boolean enabled through wp-config)

if ( constant( 'ACF_MENU' ) === false ) {
    add_filter("acf/settings/show_admin", "__return_false");
}
