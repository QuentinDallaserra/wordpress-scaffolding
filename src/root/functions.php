<?php

// Wordpress
require_once 'core/wordpress/enqueue-scripts.php';
require_once 'core/wordpress/theme-options.php';
require_once 'core/wordpress/custom-post-types.php';
require_once 'core/wordpress/custom-taxonomies.php';

// Utils
require_once 'core/utils/remove-acf-menu.php';
require_once 'core/utils/disable-gutenberg.php';
require_once 'core/utils/nl2p.php';
require_once 'core/utils/upload-svgs.php';

// Twig
require_once 'core/twig/add-to-context.php';
require_once 'core/twig/add-to-twig.php';
