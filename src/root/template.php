<?php
/*
* Template Name: Template Default Page
*/

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$templateDefaultPage = 'views/pages/template/template-default-page.twig';
Timber::render( $templateDefaultPage, $context );
