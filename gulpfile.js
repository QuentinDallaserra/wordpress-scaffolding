/*****************
 *               *
 *    Plugins    *
 *               *
 *****************/
 const gulp           = require("gulp");
 const del            = require("del");
 const sass           = require("gulp-sass");
 const browserSync    = require("browser-sync");
 const cssnano        = require("gulp-cssnano");
 const gulpIf         = require("gulp-if");
 const autoprefixer   = require("gulp-autoprefixer");
 const notify         = require("gulp-notify");
 const plumber        = require("gulp-plumber");
 const shell          = require("gulp-shell");
 const terminalColors = require("ansi-colors");
 const eslint         = require("gulp-eslint");
 const uglify         = require("gulp-uglify");
 const rename         = require("gulp-rename");
 const zip            = require("gulp-zip");
 const prompt         = require("gulp-prompt");
 const replace        = require("gulp-replace");
 const fs             = require("fs");
 const path           = require("path");
 const insert         = require("gulp-insert");
 const browserify     = require("browserify");
 const source         = require("vinyl-source-stream");
 const buffer         = require("vinyl-buffer");
 
 /**********************
  *                    *
  *    Console logs    *
  *                    *
  **********************/
 const filePathError = (file, command) => console.log(terminalColors.red(`Missing ${terminalColors.inverse(` ${file} `)} please run: ${terminalColors.yellow.inverse(` $gulp ${command} `)}`));
 
 const buildReady = () => console.log(terminalColors.green(`You are now ready to build the theme by running: ${terminalColors.green.inverse(` $gulp serve `)} or ${terminalColors.green.inverse(` $gulp build `)}`));
 
 const buildError = () => {
   if (!fs.existsSync(themeFile) && !fs.existsSync(authFile)) {
     console.log(terminalColors.red(`Theme variables undefined, nothing was built please run: ${terminalColors.yellow.inverse(` $gulp setup:init `)}`));
     process.exit();
   }
 };
 
 const isInitialised = () => {
   if (fs.existsSync(themeFile)) {
     console.log(terminalColors.red(`It looks like the theme was already initialised under the name ${terminalColors.yellow.inverse(` ${themeName} `)} it is not recommended to re-initialise the theme unless you know what you are doing`));
   }
 };
 
 /***************
  *             *
  *    Paths    *
  *             *
  ***************/
 const base = "./";
 const gulpConfig = "gulp/config/";
 const gulpTemplates = "gulp/templates/";
 const authFile = `${base}${gulpConfig}gulp-sql-auth.json`;
 const themeFile = `${base}${gulpConfig}gulp-theme.json`;
 
 try {
   var sqlAuth = require(authFile);
   var user = sqlAuth.user;
   var password = sqlAuth.password;
   var database = sqlAuth.database;
 } catch (e) {
   filePathError(authFile, "setup:wp-config");
 }
 
 try {
   var theme = require(themeFile);
   var themeName = theme.name;
   var themeSlug = theme.slug;
 } catch (e) {
   filePathError(themeFile, "setup:init");
 }
 
 const themeSrc = "src/";
 const themeDest = `wordpress/wp-content/themes/${themeSlug}/`;
 
 /*********************
  *                   *
  *    BrowserSync    *
  *                   *
  *********************/
 gulp.task("browserSync", done => {
   browserSync.init({
     host: `${themeSlug}.local`,
     notify: false,
     open: false,
   });
   done();
 });
 
 /*****************************
  *                           *
  *    Error notifications    *
  *                           *
  *****************************/
 const onError = (error) => {
   notify.onError({
     title: "FAIL",
     message: "\n\nError: <%= error.message %>\n ",
     sound: true,
   })(error);
 };
 
 /********************
  *                  *
  *    Clean dist    *
  *                  *
  ********************/
 gulp.task("clean", done => {
   del.sync([`${themeDest}/**/*`, `!${themeDest}acf-json/**`]);
   done();
 });
 
 /*******************
  *                 *
  *    Scss Task    *
  *                 *
  *******************/
 gulp.task("scss", done => {
   gulp
     .src(`${themeSrc}scss/style.scss`)
     .pipe(
       gulpIf(
         env === "dev",
         plumber({
           errorHandler: onError,
         })
       )
     )
     .pipe(sass({includePaths: ['node_modules']}))
     .pipe(autoprefixer())
     .pipe(
       gulpIf(
         env === "prod",
         cssnano({
           discardComments: false,
         })
       )
     )
     .pipe(gulp.dest(themeDest))
     .pipe(browserSync.stream());
   done();
 });
 
 /*****************
  *               *
  *    JS Task    *
  *               *
  *****************/
 gulp.task("js", done => {
   gulp.src(`${themeSrc}/js/**/*.js`)
     .pipe(eslint({}))
     .pipe(eslint.format());
 
   browserify({
     entries: `${themeSrc}js/index.js`,
     debug: true,
   })
     .transform("babelify", {
       presets: [
         [
           "@babel/preset-env",
           {
             targets: { browsers: "> 1%" },
           },
         ],
       ],
       global: true,
       ignore: ["//node_modules/(?!app/)/"],
       compact: false,
     })
     .bundle()
     .on("error", (error) => {
       gulpIf(
         env === "dev",
         plumber({
           errorHandler: onError(error),
         })
       );
     })
     .pipe(source("main.js"))
     .pipe(buffer())
     .pipe(gulpIf(env === "prod", uglify()))
     .pipe(gulp.dest(themeDest))
     .pipe(
       browserSync.reload({
         stream: true,
         once: true,
       })
     );
   done();
 });
 
 /***************************
  *                         *
  *    Font Awesome Task    *
  *                         *
  ***************************/
 gulp.task("FontAwesome", done => {
   gulp
     .src("node_modules/@fortawesome/fontawesome-pro/webfonts/*")
     .pipe(gulp.dest(`${themeDest}fonts`))
     .pipe(browserSync.stream());
   done();
 });
 
 /*******************
  *                 *
  *    Root Task    *
  *                 *
  *******************/
 gulp.task("root", done => {
   gulp.src(`${themeSrc}root/**/*`)
     .pipe(gulp.dest(themeDest))
     .pipe(browserSync.stream());
   done();
 });
 
 /******************
  *                *
  *    Zip task    *
  *                *
  ******************/
 gulp.task("zip", done => {
   gulp
     .src(["wordpress/**/*", "database/**/*"], {
       base: base,
     })
     .pipe(zip(`${themeSlug}.zip`))
     .pipe(gulp.dest(base));
   done();
 });
 
 /***********************
  *                     *
  *    Task iterator    *
  *                     *
  ***********************/
 const getFolders = (directory) => {
   return fs.readdirSync(directory).filter((file) => {
     return fs.statSync(path.join(directory, file)).isDirectory();
   });
 };
 
 const srcFolders = getFolders(themeSrc);
 const excludedFolders = ["scss", "js", "root"];
 
 partialFolders = srcFolders.filter((index) => {
   return excludedFolders.indexOf(index) === -1;
 });
 
 partialFolders.forEach((folder) => {
   return gulp.task(folder, () => {
    return gulp
       .src(`${themeSrc}${folder}/**/*`)
       .pipe(gulp.dest(`${themeDest}${folder}`))
       .pipe(
         browserSync.reload({
           stream: true,
           once: true,
         })
       );
   });
 });
 
 /******************
  *                *
  *    SQL Task    *
  *                *
  ******************/
 const sqlSync = (command, stream, direction) => {
   gulp.task(`sql:${stream}`, done => {
     gulp
       .src("database/")
       .pipe(
         plumber({
           errorHandler: onError,
         })
       )
       .pipe(shell(`${command} --user=${user} --password=${password} ${database} ${direction} database/${themeSlug}.sql`));
     done();
   });
 };
 
 sqlSync("mysqldump", "dump", ">");
 sqlSync("mysql", "push", "<");
 
 /***************************
  *                         *
  *    Gulp: development    *
  *                         *
  ***************************/
 gulp.task("serve", done => {
   env = "dev";
   const runServeTask = gulp.series("clean", "FontAwesome", gulp.parallel(srcFolders), "browserSync");
   buildError();
   runServeTask();
   srcFolders.forEach((folder) => {
     gulp.watch(`${themeSrc}${folder}/**/*`, gulp.series([folder]));
   });
   done();
 });
 
 /**************************
  *                        *
  *    Gulp: production    *
  *                        *
  **************************/
 gulp.task("build", done => {
   env = "prod";
   const runBuildTask = gulp.series("clean", "FontAwesome", srcFolders);
   buildError();
   runBuildTask();
   done();
 });
 
 /***********************
  *                     *
  *    Gulp: compile    *
  *                     *
  ***********************/
 gulp.task("compile", done => {
   const runCompileTask = gulp.series("clean", gulp.parallel("build", "sql:dump"), "zip");
   buildError();
   runCompileTask();
   done();
 });
 
 /************************
  *                      *
  *    Setup: prompts    *
  *                      *
  ************************/
 const createAuthFile = (dbName, dbUser, dbPassword) => {
   gulp.src(`${gulpTemplates}template_gulp-sql-auth.json`)
     .pipe(rename("gulp-sql-auth.json"))
     .pipe(replace("$dbName", dbName))
     .pipe(replace("$dbUser", dbUser))
     .pipe(replace("$dbPassword", dbPassword))
     .pipe(gulp.dest(gulpConfig));
 };
 
 const createWpConfig = (dbName, dbUser, dbPassword, dbHost, wpDebug, browserSync, customFields) => {
   gulp.src(`${gulpTemplates}template_wp-config.php`)
     .pipe(rename("wp-config.php"))
     .pipe(replace("$dbName", dbName))
     .pipe(replace("$dbUser", dbUser))
     .pipe(replace("$dbPassword", dbPassword))
     .pipe(replace("$dbHost", dbHost))
     .pipe(replace("$wpDebug", wpDebug))
     .pipe(replace("$browserSync", browserSync))
     .pipe(replace("$customFields", customFields))
     .pipe(gulp.dest("wordpress/"));
 };
 
 const createThemeDescription = (themeName, themeSlug, themeAuthor, themeUrl) => {
   gulp.src(`${gulpTemplates}template_theme-description.scss`)
     .pipe(rename("theme-description.scss"))
     .pipe(replace("$themeName", themeName))
     .pipe(replace("$themeAuthor", themeAuthor))
     .pipe(replace("$themeUrl", themeUrl))
     .pipe(gulp.dest("src/scss"));
   
   gulp.src(`${gulpTemplates}template_gulp-theme.json`)
     .pipe(rename("gulp-theme.json"))
     .pipe(replace("$themeSlug", themeSlug))
     .pipe(replace("$themeName", themeName))
     .pipe(gulp.dest(gulpConfig));
   
   gulp
     .src(`${gulpTemplates}template_database.sql`)
     .pipe(replace("$themeSlug", themeSlug))
     .pipe(rename(`${themeSlug}.sql`))
     .pipe(gulp.dest("database/"));
 
   gulp.src(`${gulpTemplates}template_readme.md`)
     .pipe(rename("README.md"))
     .pipe(replace("$themeName", themeName))
     .pipe(replace("$themeSlug", themeSlug))
     .pipe(replace("$themeAuthor", themeAuthor))
     .pipe(replace("$themeUrl", themeUrl))
     .pipe(gulp.dest(base));
 };
 
 const addThemeToGitignore = (themeSlug) => {
   gulp
     .src(".gitignore", {
       dot: true,
     })
     .pipe(insert.prepend(`wordpress/wp-content/themes/${themeSlug}/*\n!wordpress/wp-content/themes/${themeSlug}/acf-json/\n`))
     .pipe(gulp.dest(base));
 };
 
 const createAcfDirectory = (themeSlug) => {
   gulp.src("*.*", { read: false }).pipe(gulp.dest(`wordpress/wp-content/themes/${themeSlug}/acf-json`));
 };
 
 /**************************
  *                        *
  *    Setup: wp-config    *
  *                        *
  **************************/
 gulp.task("setup:wp-config", done => {
   gulp
     .src(base)
     .pipe(
       prompt.confirm({
         message: "Setup wp-config?",
         default: true,
       })
     )
     .pipe(
       prompt.prompt(
         [
           {
             type: "input",
             name: "dbName",
             message: "Database name?",
           },
           {
             type: "input",
             name: "dbUser",
             message: "Database user name?",
           },
           {
             type: "password",
             name: "dbPassword",
             message: "Database password?",
           },
           {
             type: "input",
             name: "dbHost",
             message: "Database host IP? Default:",
             default: "localhost",
           },
           {
             type: "list",
             name: "wpDebug",
             message: "Enable WP_DEBUG?",
             choices: ["true", "false"],
             default: "false",
           },
           {
             type: "list",
             name: "browserSync",
             message: "Enable BROWSER_SYNC?",
             choices: ["true", "false"],
             default: "false",
           },
           {
             type: "list",
             name: "customFields",
             message: "Show ACF_MENU?",
             choices: ["true", "false"],
             default: "false",
           },
         ],
         (response) => {
           createWpConfig(response.dbName, response.dbUser, response.dbPassword, response.dbHost, response.wpDebug, response.browserSync, response.customFields);
           createAuthFile(response.dbName, response.dbUser, response.dbPassword);
           done();
         }
       )
     );
 });
 
 /**********************
  *                    *
  *    Setup: theme    *
  *                    *
  **********************/
 gulp.task("setup:theme", done => {
   isInitialised();
 
   gulp
     .src(base)
     .pipe(
       prompt.confirm({
         message: "Setup theme description?",
         default: true,
       })
     )
     .pipe(
       prompt.prompt(
         [
           {
             type: "input",
             name: "themeName",
             message: "Theme name?",
           },
           {
             type: "input",
             name: "themeSlug",
             message: "Theme slug?",
           },
           {
             type: "input",
             name: "themeAuthor",
             message: "Author name?",
           },
           {
             type: "input",
             name: "themeUrl",
             message: "Author URL",
           },
         ],
         (response) => {
           createThemeDescription(response.themeName, response.themeSlug, response.themeAuthor, response.themeUrl);
           addThemeToGitignore(response.themeSlug);
           createAcfDirectory(response.themeSlug);
           buildReady();
           done();
         }
       )
     );
 });
 
 /*********************
  *                   *
  *    Setup: full    *
  *                   *
  *********************/
 gulp.task("setup:init", done => {
   const runCompleteSetup = gulp.series("setup:wp-config", "setup:theme");
   isInitialised();
   runCompleteSetup();
   done();
 });
