
# $themeName

A $themeName site by [$themeAuthor]($themeUrl)

  

### Tech/Plugins:

$themeName uses the following core technologies/plugins to work properly:

  

*  [Wordpress](https://wordpress.org/) - Duh

*  [Twig](https://twig.symfony.com/) - A flexible, fast, and secure template engine for PHP

*  [Advanced Custom Fields](https://www.advancedcustomfields.com/) - A WordPress plugin which allows you to add extra content fields

*  [Gulp](http://gulpjs.com/) - A streaming build system

*  [Jenkins](https://jenkins.io/) - Open source automation server

*  [Font Awesome](https://fontawesome.com/) - Icon Library

*  [NodeJS (Version 12)](https://nodejs.org/en/) - A JavaScript runtime built on Chrome's V8 JavaScript engine
  
*  [Browserify](http://browserify.org/) - Browserify lets you require('modules') in the browser by bundling up all of your dependencies.

*  [Browsersync](https://browsersync.io/) - Time-saving synchronised browser testing and hot reload

* And more.


---


### Installation

Git clone the $themeName repo:

```sh

$ git clone https://bitbucket.org/$themeSlug.git

```

  

Install the the [NPM](https://www.npmjs.com/) dependencies into the ```$themeSlug``` root directory. (You will need to have [Node.js](https://nodejs.org/en/) installed on your machine.).

  

```sh

$ cd $themeSlug

$ npm install

```

  
---


### Development

This site uses [Gulp](http://gulpjs.com/) for development automation. Gulp will build ```$themeSlug/src``` into ```$themeSlug/wordpress/wp-content/themes/$themeSlug``` and delete the latter folder for a clean build every time Gulp is run (**note:** ```acf-json``` located inside the theme folder will not be deleted as it is needed to sync custom fields between local and live databases). Because ```$themeSlug/wordpress/wp-content/themes/$themeSlug``` is gitignored and deleted every time gulp is run it is important that you develop from ```$themeSlug/src```.

  
---


##### Setup your local server

Point your xammp/mamp/wamp or a local server of your preference to the ```$themeSlug/wordpress``` directory.

Use ```http://$themeSlug.local``` as your development URL.

  

##### Building for development

For development:

```sh

$ cd $themeSlug

$ gulp serve

```

  

This will delete ```$themeSlug/wordpress/wp-content/themes/$themeSlug/**/*``` (but keep the ```acf-json``` directory intact enabling database custom fields synchronisation) for a clean build and rebuild the following, additionally it will watch all files for changes and live reload in the browser using [Browsersync](https://www.browsersync.io/):

  

- ESlint and bundle Javascript using Browserify from the index.js entry point ```$themeSlug/src/js/index.js``` into ```$themeSlug/wordpress/wp-content/themes/$themeSlug/main.js```  

  

- copy Sass and autoprefix from style.css ```$themeSlug/src/scss/style.scss``` into ```$themeSlug/wordpress/wp-content/themes/$themeSlug/style.css```

  

-  **Important:** the gulp script uses a custom iterator, any folder placed inside ```src``` will automatically be watched and copied to the root of ```$themeSlug/wordpress/wp-content/themes/$themeSlug/```. This currently includes: ```src/fonts```, ```src/img``` and ```src/views```. You can specify which directory you do not want to follow this pattern by adding it into this array in the gulpfile.js: ```var excludedFolders = ['scss', 'js', 'root'];```

  

##### Building for staging/live

For staging/live:

```sh

$ cd $themeSlug

$ git add .

$ git commit -a -m "Commit message"

$ git push origin master

```

  

This website uses [Jenkins](https://jenkins.io/) for continuous deployment. ```$git push origin master``` will automatically deploy the latest commit on the master branch to the staging server. Jenkins will then run ```$gulp build```. This will excecute identical to ```$gulp serve```. Additionally it will minify ```main.js``` and ```style.css```. It will also run various production enhancement found in gulpfile.js.

  

##### Development flowchart:

_Need to update flowchart after structure refactor_

  
---


### Gulp commands

-  ```$gulp compile``` will build the ```src``` folder into ```$themeSlug/wordpress/wp-content/themes/```. It will then package and compile the complete wordpress project and include a dump of your latest local database into a zip file named ```$themeSlug.zip``` in the root directory

  

-  ```$gulp build``` will compile a ready for production ```src``` into ```$themeSlug/wordpress/wp-content/themes``` as previously mentioned above in the _Building for staging/live_ example

  

-  ```$gulp serve``` will compile a ready for development ```src``` into ```$themeSlug/wordpress/wp-content/themes``` as previously mentioned above in the _Building for development_ example

  

-  ```$gulp setup:wp-config``` will run you through a command prompt with multiple questions. It will use the prompted answers to create a ```wp-config.php``` file (including different needed variables for the smooth operation of the site such as enabling/disabling BrowserSync etc...) and will also create a ```gulp-sql-auth.json``` required for the SQL dump/push functionality seen below. Alternatively you may copy the ```$themeSlug/wordpress/wp-config-sample.php``` file to ```$themeSlug/wordpress/wp-config.php```

  

-  ```$gulp sql:dump``` will backup your local database inside the ```$themeSlug/database``` directory

  

-  ```$gulp sql:push``` will push ```$themeSlug/database/$themeSlug.sql``` into your mysql database

  

-  ```$gulp setup:theme``` and ```$gulp setup:init```  **Important:** these functionalities are used to setup the initial wordpress scafolding. If you are seeing this readme it means that the theme ```$themeName``` has already been initialised therefore it is **strongly** advised not to run these commands unless you know what you are doing as it will overwrite various files and inject prompted answers into different variables

  
---


### Important Extras

- Wordpress backoffice username: ```Admin``` and password: ```changeme``` (Those credentials are used at the initial project build. If you are getting authentication failures you will find the updated credentials on KeePass).



- Wordpress requires certain files to be places in the theme root directory (such as function.php etc...). These types of files should be placed in ```$themeSlug/src/root/```.



- Font Awesome is baked into this build, **Usage:** [Font Awesome guide](https://fontawesome.com/how-to-use/on-the-web/referencing-icons/basic-use).



- The theme ```$themeSlug/wordpress/wp-content/themes/$themeSlug``` is git ignored therefore you will have to ```$gulp build``` or ```$gulp serve``` to generate it from the ```src``` directory

  

- The file ```$themeSlug/wordpress/wp-config``` is git ignored, you will have to create your own by either running: ```$gulp setup:wp-config``` **(strongly advised)** or using the usual manual methods by copying the ```$themeSlug/wordpress/wp-config-sample.php``` file to ```$themeSlug/wordpress/wp-config.php```. Two important fields have been added: 

    - ```define( 'BROWSER_SYNC', boolean )``` Enables or disables browsersync (Disable for production and enable for local development)
    - ```define( 'ACF_MENU', boolean )``` Hides the ACF menu from the CMS (To prevent a customer making unwanted changes on a production site).

  

- Instead of the default twig ```{{ dump(variable) }}``` which will dump on the page, You can log to chrome dev-tools console with ```{{ console(variable) }}```

  
---


### Contacts

-  [$themeAuthor]($themeUrl)
